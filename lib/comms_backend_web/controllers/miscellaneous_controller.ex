defmodule CommsWeb.MiscellaneousController do
  use CommsWeb, :controller
  alias Comms.Miscellaneous

  def comments(conn, params) do
    with :ok <- Miscellaneous.comments(params) do
      conn
      |> send_resp(200, "OK")
    end
  end
end
