defmodule Comms.Relax do
  def create_doc(db_name, doc) do
    doc_with_ts = doc |> insert_created_at()

    db_name
    |> get_db()
    |> ICouch.save_doc(doc_with_ts)
  end

  def create_docs(db_name, docs) do
    docs_with_ts =
      docs
      |> Enum.map(fn doc ->
        doc |> insert_created_at()
      end)

    db_name
    |> get_db()
    |> ICouch.save_docs(docs_with_ts)
  end

  def retrieve_doc(db_name, doc_id) do
    db_name
    |> get_db()
    |> ICouch.open_doc(doc_id)
  end

  def update_doc(db_name, doc) do
    doc_with_ts = doc |> insert_updated_at()

    db_name
    |> get_db()
    |> ICouch.save_doc(doc_with_ts)
  end

  def delete_doc(db_name, doc_id) do
    db_name
    |> get_db()
    |> ICouch.delete_doc(doc_id)
  end

  def matching_views(db_name, design_doc_and_view, options \\ []) do
    db_name
    |> get_db()
    |> ICouch.open_view!(design_doc_and_view, options)
    |> ICouch.View.fetch!()
    |> Map.from_struct()
    |> Map.fetch!(:rows)
  end

  def mango_query(db_name, query) do
    get_server_info()
    |> ICouch.Server.send_req(
      {"#{db_name}/_find", []},
      :post,
      query
    )
    |> elem(1)
  end

  defp insert_created_at(doc) do
    doc
    |> Map.put("created_at", current_time_utc())
  end

  defp insert_updated_at(doc) do
    doc
    |> Map.put(:inserted_at, current_time_utc())
  end

  defp current_time_utc do
    Timex.now()
    |> Timex.to_datetime()
    |> Timex.format!("{ISO:Extended}")
  end

  defp get_db(db_name) do
    get_server_info()
    |> ICouch.open_db!(db_name)
  end

  defp get_server_info() do
    couchdb_env = Application.get_env(:comms_backend, Comms.Relax)

    couchdb_ip = couchdb_env |> Keyword.fetch!(:couchdb_ip)
    couchdb_port = couchdb_env |> Keyword.fetch!(:couchdb_port)
    couchdb_username = couchdb_env |> Keyword.fetch!(:couchdb_username)
    couchdb_password = couchdb_env |> Keyword.fetch!(:couchdb_password)

    uri = "http://" <> couchdb_ip <> ":" <> couchdb_port

    options = [
      basic_auth: {couchdb_username, couchdb_password}
    ]

    ICouch.server_connection(uri, options)
  end
end
