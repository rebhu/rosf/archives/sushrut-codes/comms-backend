defmodule Comms.Data do
  alias Comms.Relax
  @data "data"
  @welcome_doc %{
    _id: "custom",
    patient_sex: "",
    patient_age: "-> Strategies by/for/of healthcare practitioners <",
    drug_name: "Fight against COVID-19",
    disease_name: "COVID-19",
    description:
      "Thank you for your service to humanity. We built this to help healthcare practitioners get tips from those at the frontline of this COVID-19 pandemic and vice versa. Click on the button with plus icon at the bottom and share strategies that have helped you improve patient condition. This will help come up with treatment procedures to mitigate and control the global pandemic. We built this to let you know that you are not alone in this war against COVID-19. This is an open source effort to determine medical procedures and engineer a cure for COVID-19 spread by SARS-CoV2. Switch to landscape mode for requesting features, feedback, and comments.",
    created_at: "You are not alone in this"
  }

  def list(attrs) do
    total_displayed_records = attrs["total_displayed_records"]

    options =
      cond do
        total_displayed_records |> is_nil() ->
          []

        total_displayed_records
        |> String.to_integer()
        |> is_integer() ->
          [skip: total_displayed_records |> String.to_integer()]
      end ++
        [
          descending: true,
          limit: 2
        ]

    @data
    |> Relax.matching_views(
      "organize/post-view",
      options
    )
    |> Enum.map(fn map ->
      Relax.retrieve_doc(@data, map["id"])
      |> elem(1)
      |> Map.fetch!(:fields)
    end)
    |> case do
      [] ->
        [@welcome_doc]

      docs ->
        docs
    end
  end

  # def show(attrs) do
  # end

  def create(attrs) do
    @data
    |> Relax.create_doc(attrs)
  end

  # def update(attrs) do
  # end

  # def remove(attrs) do
  # end

  def list_endorsement(attrs) do
    @data
    |> Relax.matching_views(
      "organize/net-endorsement-view",
      key: attrs["data_id"]
    )
    |> List.first()
  end

  def show_endorsement(attrs) do
    @data
    |> Relax.matching_views(
      "organize/creator-endorsement-view",
      key: [attrs["creator_id"], attrs["data_id"]]
    )
  end

  def create_endorsement(attrs) do
    # Check if the endorsement already exists
    # Take appropriate action based on this
    with [] <- attrs |> show_endorsement() do
      @data
      |> Relax.create_doc(attrs)
    else
      [endorsement] ->
        updated_endorsement =
          endorsement["value"]
          |> handle_endorsement(attrs["endorsement_value"])

        doc =
          Relax.retrieve_doc(@data, endorsement["id"])
          |> elem(1)
          |> Map.from_struct()
          |> Map.get(:fields)

        @data
        |> Relax.update_doc(%{doc | "endorsement_value" => updated_endorsement})
    end
  end

  defp handle_endorsement(old, new) do
    cond do
      (old == 0 && new == 1) || (old == -1 && new == 1) ->
        1

      (old == -1 && new == -1) || (old == 1 && new == 1) ->
        0

      (old == 1 && new == -1) || (old == 0 && new == -1) ->
        -1
    end
  end
end
