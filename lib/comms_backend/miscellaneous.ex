defmodule Comms.Miscellaneous do
  alias Comms.Relax

  @miscellaneous "miscellaneous"

  def comments(attrs) do
    case Relax.create_doc(@miscellaneous, attrs) do
      {:ok, _} ->
        :ok

      _ ->
        :error
    end
  end
end
