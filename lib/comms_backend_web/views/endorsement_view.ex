defmodule CommsWeb.EndorsementView do
  use CommsWeb, :view

  alias CommsWeb.EndorsementView

  def render("show.json", %{endorsement: endorsement}) do
    %{endorsement: render_one(endorsement, EndorsementView, "page.json")}
  end

  def render("page.json", %{endorsement: endorsement}) do
    endorsement
    |> Map.drop(["_rev"])
  end
end
