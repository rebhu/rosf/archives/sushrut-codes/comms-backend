defmodule CommsWeb.DataView do
  use CommsWeb, :view
  alias CommsWeb.DataView

  def render("index.json", %{datum: datum}) do
    %{data: render_many(datum, DataView, "page.json")}
  end

  def render("show.json", %{data: data}) do
    %{data: render_one(data, DataView, "page.json")}
  end

  def render("page.json", %{data: data}) do
    data
    |> Map.drop(["_rev"])
  end
end
