defmodule CommsWeb.Router do
  use CommsWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug CommsWeb.Plugs.ID, []
  end

  scope "/data", CommsWeb do
    pipe_through :api

    head "/", DataController, :headers
    resources "/", DataController, except: [:new, :edit] do
      resources "/endorsement", EndorsementController, only: [:index, :show, :create]
    end
  end

  scope "/miscellaneous", CommsWeb do
    pipe_through :api

    post "/comment", MiscellaneousController, :comments
  end
end
